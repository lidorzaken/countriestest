export const GET_COUNTRIES = "GET_COUNTRIES", SET_COUNTRIES = "SET_COUNTRIES", SET_DISPLAY_COUNTRIES = "SET_DISPLAY_COUNTRIES";

export const getCountries = () => ({
    type: GET_COUNTRIES
})

export const setCountries = (countries) => ({
    type: SET_COUNTRIES,
    countries
})

export const setDisplayCountries = (letter, countries) => ({
    type: SET_DISPLAY_COUNTRIES,
    letter,
    countries
})