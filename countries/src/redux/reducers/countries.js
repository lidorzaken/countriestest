import CountriesList from "../../models/countriesList";
import { countriesAction } from "../actions";

const initialState = {
    countries: {},
    displayCountries: []
}

export default function countries(state = initialState, action) {
    switch (action.type) {
    case countriesAction.SET_COUNTRIES:
        return {...state, countries: new CountriesList(action.countries)};
    
        case countriesAction.SET_DISPLAY_COUNTRIES:
            return {...state, displayCountries: [...state.displayCountries, {letter: action.letter, list: state.countries.getListByFirstLetter()[action.letter]}]};

   default:
        return state;
    }
  }