import { call, put } from 'redux-saga/effects';
import { countriesAction } from '../../actions';
import { countriesRequests } from '../requests';

export function* handleGetCountries (action) {
    try {
        const response = yield call(countriesRequests.requestGetCountries)
        const { data } = response;
        yield put(countriesAction.setCountries(data))
    } catch (error) {
        console.log(error)
    }
}