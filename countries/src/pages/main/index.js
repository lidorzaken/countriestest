import React, {useState} from 'react';
import { useSelector, shallowEqual  } from 'react-redux';
import SideMenu from './sideMenu';
import List from './list';
import { useScrollIntoView } from '../../myHooks';

import Header from './header';

const Index = () => {
    const [ref, scrollIntoView] = useScrollIntoView();
    const [letterToShow, setLetterToShow] = useState('');
    const { countries } = useSelector(
        state => ({
          countries: state.countries.displayCountries
        }),
        shallowEqual
      );

    return (
        <div className="w-full">
            <Header scrollIntoView={scrollIntoView}/>
            <div ref={ref} className="flex h-screen">
                <SideMenu countries={countries} setLetterToShow={setLetterToShow}/>
                <List firstLetterLoaded={countries.length>0} setLetterToShow={setLetterToShow} countries={letterToShow!==''?countries.find(list=>list.letter===letterToShow):[]}/>
            </div>
        </div>
    )
}

export default Index


