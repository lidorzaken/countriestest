import React from 'react'

const Index = ({country}) => {
    return (
        <div className="border popText w-2/3 flex flex-col space-y-2 items-center shadow-lg border-blue-100 rounded-xl p-4 md:p-8 xl:p-10">
            <img className="h-10 lg:h-20 w-auto rounded-lg shadow-lg" src={country.getFlag()} alt={country.getName} />
            <h3 className="text-lg lg:text-3xl text-blue-900">{country.getName()}</h3>
            <div className="flex space-x-8">
                <p>{`Capital: ${country.getCapital()}`}</p>
                <p>{`Population: ${country.getPopulation()}`}</p>
                <p>{`alpha2Code: ${country.getCode()}`}</p>
            </div>
        </div>
    )
}

export default Index
