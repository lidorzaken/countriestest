import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector, shallowEqual  } from 'react-redux';
import { countriesAction } from '../redux/actions';

const Index = ({children}) => {
    const [initialList, setInitialList] = useState({})
    const sorted = Object.keys(initialList).sort()

    const FIFTHEEN_SECONDS = 15;

    const { countries } = useSelector(
        state => ({
          countries: state.countries.countries
        }),
        shallowEqual
      );

      const start = (counter) => {
        if(counter < Object.keys(initialList).length){
          setTimeout(function(){
            dispatch(countriesAction.setDisplayCountries(sorted[counter]))
            counter++;
            start(counter);
          }, FIFTHEEN_SECONDS * 1000);
        }
      }


     useEffect(() => {
        if(Object.keys(countries).length > 0){ 
            setInitialList(countries.getListByFirstLetter());
        }
     }, [countries])

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(countriesAction.getCountries())
    }, [dispatch])

    if(Object.keys(countries).length > 0){
        start(0);
        }

    return (
        <>
            {children} 
        </>
    )
}

export default Index
