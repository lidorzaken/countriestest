import { takeLatest } from 'redux-saga/effects';
import { countriesAction } from '../actions';
import {countriesHandler} from './handlers';

export function* watcherSaga() {
    yield takeLatest(countriesAction.GET_COUNTRIES, countriesHandler.handleGetCountries)
}