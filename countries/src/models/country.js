class Country {
    constructor({
        name,
        population,
        flags,
        cca2,
        capital
    }) {
      this.name = name;
      this.population = population;
      this.flags = flags;
      this.alpha2Code = cca2;
      this.capital = capital
    }

    getName = () => {
        return this.name.common;
    }
    getPopulation = () => {
        return this.population;
    }
    getFlag = () => {
        return this.flags.png;
    }
    getCapital = () => {
        return this.capital;
    }
    getCode = () => {
        return this.alpha2Code;
    }
  
  }
  
  export default Country;
  