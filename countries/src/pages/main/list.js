import React, {useState, useEffect, useCallback} from 'react';
import Card from '../../components/card';
import Search from '../../components/searchBar';
import CountriesList from "../../models/countriesList";

const Index = ({countries, setLetterToShow, firstLetterLoaded}) => {

    const [filtered, setFiltered] = useState([]);
    const [wordsOfSearch, setWordsOfSearch] = useState([]);
      const [searchLists, setSearchLists] = useState({});

    const getListsForSearch = useCallback(() => {
        const namesList = countries?.list
          .map( country => country.getName())

    
        return {
            namesList: [...new Set(namesList)]
        };
      }, [countries]);
    
      const searchWordsHandler = text => {
        let codeTemp = [],
          nameTemp = [],
          capitalTemp = [];
        const wordsArray = text
          .toLowerCase()
          .split(' ')
          .filter(word => word.trim() !== '');
        wordsArray.forEach(word => {
          if (word.length > 0) {
            searchLists.namesList.forEach(name => {
              if (name.toLowerCase().includes(word)) {
                nameTemp.push(name);
              }
            });
          }
        });
    
        setWordsOfSearch([
          ...new Set(codeTemp),
          ...new Set(capitalTemp),
          ...new Set(nameTemp)
        ]);
      };
      useEffect(() => {
        firstLetterLoaded && setLetterToShow('A');
        firstLetterLoaded && Object.keys(countries).length>0 && setSearchLists(getListsForSearch());
        if(Object.keys(countries).length>0) {
            setFiltered(new CountriesList(countries.list))
            }
      }, [firstLetterLoaded, getListsForSearch, setSearchLists, countries, setLetterToShow]);

    return (
        <div className="w-10/12 h-full overflow-y-auto">
            {Object.keys(filtered).length>0?
            <div>
                <Search onTypeHandler={searchWordsHandler} placeHolder="Search by Name" className="w-full bg-white h-20 text-xl text-blue-900 p-4 border-b border-blue-900"/>
                <div className="flex flex-col space-y-6 p-20">
                    {(wordsOfSearch.length>0?filtered.getFilteredList(wordsOfSearch):filtered.getList()).map((country,key)=>{
                    return <Card key={`CardCountryKey_${key}`} country={country}/>
                })}
                </div>
            </div>
        :<div className="flex justify-center py-20"><h5>Sorry, we don't discover any new countries yet.</h5></div>}
        </div>
    )
}

export default Index;
