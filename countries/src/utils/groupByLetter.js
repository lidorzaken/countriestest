Array.groupByLetter = function groupBy(array, by) {
const keys = by.split('.');
  const getKey = (obj, deepens = 0) => {
    if (deepens === keys.length - 1) return obj[keys[deepens]].charAt(0);
    else return getKey(obj[keys[deepens]], deepens + 1).charAt(0);
  };

  return array.reduce((acc, obj) => {
    const key = getKey(obj);
    acc[key] = acc[key] || [];
    acc[key].push(obj);
    return acc;
  }, {});
};
