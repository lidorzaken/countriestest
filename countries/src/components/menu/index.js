import React from 'react'

const Index = ({letter, setLetterToShow}) => {
    return (
        <div onClick={()=>{setLetterToShow(letter)}} className="px-4 py-4 hover:bg-yellow-500 border-b border-blue-900 transform transition duration-500 ease-in-out cursor-pointer flex justify-center items-center">
            <h4 className="text-blue-900">{letter}</h4>
        </div>
    )
}

export default Index
