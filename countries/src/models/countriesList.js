import Country from './country';

class CountriesList {
  constructor(countries) {
    this.list = countries.map(el => new Country(el));
  }

  getList() {
    return this.list;
  }

  getListByNames() {
    return Array.groupBy(this.list, 'name.common');
  }

  getListByFirstLetter() {
    return Array.groupByLetter(this.list, 'name.common');
  }

  getFilteredList(wordsOfSearch) {
    const temp = [];
    const filtered = [];
    this.list.forEach(country => {
      let element = [];
      element.push(country.getName());
      temp.push(element);
    });
    temp.forEach((element, index) => {
      if (wordsOfSearch.some(word => element.includes(word))) {
        filtered.push(this.list[index]);
      }
    });
    return filtered;
  }

}
export default CountriesList;