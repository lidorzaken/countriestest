import {createStore , applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './redux/reducers';
import { watcherSaga } from './redux/sagas/rootSaga';

const sagaMiddleware = createSagaMiddleware();

const middlewaresList = [sagaMiddleware];

export const store = createStore(
  rootReducer, {}, applyMiddleware(...middlewaresList)
)

sagaMiddleware.run(watcherSaga);