import React from 'react';
import MenuCard from '../../components/menu'

const Index = ({countries, setLetterToShow}) => {
    return (
        <div className="w-2/12 bg-yellow-400 h-full flex flex-col overflow-y-auto">
            {countries.length > 0 && countries.map((element,key)=>{
                return <MenuCard key={`keyOfMenuElement_${key}`} letter={element.letter} setLetterToShow={setLetterToShow}/>
            })}
            
        </div>
    )
}

export default Index;
