import React from 'react';
import { BiGlobe } from 'react-icons/bi';

const Index = ({scrollIntoView}) => {
    return (
        <div className="w-full px-5 md:px-10 lg:px-20 xl:px-40 flex items-center h-screen bg-blue-900 text-white">
            <div className="flex flex-col space-y-8">
                <span className="flex flex-wrap text-5xl md:text-6xl xl:text-8xl space-x-3 items-center"><h1 className="text-5xl md:text-6xl xl:text-8xl subtitlePop popText">Welcome</h1><h1 className="text-5xl md:text-6xl xl:text-8xl titlePop popText">to</h1><h1 className="text-5xl md:text-6xl xl:text-8xl subtextPop popText">Country</h1><span className="flex space-x-3 text-5xl md:text-6xl xl:text-8xl subtextPop popText"><h1 className="text-5xl md:text-6xl xl:text-8xl">Finder</h1> <BiGlobe /></span></span>
                <button onClick={()=>scrollIntoView()} className="button1Pop hover:scale-125 hover:bg-yellow-400 transform transition duration-500 ease-in-out cursor-pointer popText bg-white text-xl hover:bg-gray-100 w-48 rounded-xl px-10 py-2 text-blue-900 text-center">Explore now</button>
            </div>
        </div>
    )
}

export default Index
