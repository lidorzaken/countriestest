import React from 'react';

const Index = ({
  onTypeHandler,
  placeHolder,
  className
}) => {


  return (
    <>
      <input
        onChange={e => {
          onTypeHandler(e.target.value);
        }}
        placeholder={placeHolder}
        type="text"
        className={`relative ${className}`}
      />
    </>
  );
};

export default Index;
